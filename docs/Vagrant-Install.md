# Vagrant Setup

## 1. start the VM

	vagrant up

## 2. HostManager configuration

	vagrant hostmanager 

## 3. SSH Connect

	vagrant ssh

SSH Connect should bring you at the root of the project

## 4. Activate virtualenv

	source /srv/.virtualenvs/{{ PROJECT_NAME }}/bin/activate

## 5. Sync DB and create admin user

Create an admin user

	./manage.py syncdb

Then make sure all models a migrated

	./manage.py migrate

## 5. Start server

	./manage.py runserver 0.0.0.0:8000

## 6. In your browser

Point your browser to the Vagrant server, check host's HTTP port used in local VagranFile

	http://localhost:{{ PORT_NUMBER }}/

## 7. Profit!