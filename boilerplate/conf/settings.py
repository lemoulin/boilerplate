from .settings_base import *

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


# SECURITY WARNING: keep the secret key used in production secret!
# Use bin/secret_key_generator.py to generate a SECRET_KEY
SECRET_KEY = 'amyjmckjd17+$t9$-u-86(nolf&7vwvh=lb^(s_+xbji0d4ma6'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
THUMBNAIL_DEBUG = DEBUG
SHUTDOWN_SITE = False

ALLOWED_HOSTS = ["boilerplate.yanikproulx.com", ]

ADMINS = (
	('Admin', 'yanikproulx@lemoulin.co'),
)

# Mail settings
SERVER_EMAIL = "boilerplate Server <server@boilerplate.yanikproulx.com>"
DEFAULT_FROM_EMAIL = "boilerplate <noreply@boilerplate.yanikproulx.com>"
CONTACT_US_EMAIL = "yanikproulx@lemoulin.co"

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = "localhost"
EMAIL_PORT = 25
# EMAIL_HOST = "smtp.mandrillapp.com"
# EMAIL_PORT = 587
# EMAIL_HOST_USER = "thetoine@gmail.com"
# EMAIL_HOST_PASSWORD = ""	# Generate Mandrill API key
# EMAIL_USE_TLS = True

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql_psycopg2',	 # 'postgresql_psycopg2', 'mysql',
		'NAME': 'boilerplate',
		'CONN_MAX_AGE': 0,
		'USER': 'boilerplate',
		'PASSWORD': '91RXlPE5cb6t',
		'HOST': 'localhost',
		'PORT': '',
	}
}

TEMPLATES[0]["DIRS"] = [os.path.join(BASE_DIR, "boilerplate", "templates/")]

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(BASE_DIR, "static/")

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/
STATIC_URL = '/static/'

# Grappelli
GRAPPELLI_URL = STATIC_URL + "grappelli/"
GRAPPELLI_INDEX_DASHBOARD = "boilerplate.conf.dashboard.CustomIndexDashboard"

# URL that handles the media served from MEDIA_ROOT, used for managing stored files. It must end in a slash if set to a non-empty value.
# https://docs.djangoproject.com/en/1.9/ref/settings/#media-url
MEDIA_URL = '/media/'

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media/")

LOCALE_PATHS = (
	os.path.join(BASE_DIR, "locale/"),
)


# Django Compressor settings
COMPRESS_ENABLED = True
COMPRESS_CSS_HASHING_METHOD = 'content'  # not using mtime since it differs between servers.
COMPRESS_REBUILD_TIMEOUT = 3600  # recompress every hour

LESSC_PATH = "/srv/www/boilerplate/node_modules/less/bin/lessc"

COMPRESS_PRECOMPILERS = (
	('text/less', str("{LESSC_PATH} {{infile}} {{outfile}}".format(LESSC_PATH=LESSC_PATH))),
)

# Memcached, if necessary
#CACHES = {
#		'default': {
#				'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#				'LOCATION': '127.0.0.1:11211',
#				'KEY_PREFIX': 'nationalnoel',
#		}
#}
