from django.db import models
from django.utils.translation import ugettext_lazy as _

from lemoulin_extras.l10n.models import ModelWithTranslations, MultilingualModel
from lemoulin_extras.gallery.models import Media


def get_product_files_path(instance, filename):
	from lemoulin_extras.utils.files import clean_filename

	return "base/products/{filename}".format(filename=clean_filename(filename))


class Product(ModelWithTranslations):
	internal_name = models.CharField(verbose_name=_("Internal Name*"), help_text=_("*Only used internally. The translated name will be used on the site."), max_length=256, )
	slug = models.SlugField(verbose_name=_("Slug"), max_length=64, unique=True, )
	image = models.ImageField(verbose_name=_("Image"), upload_to=get_product_files_path, null=True, blank=True, )
	order = models.PositiveIntegerField(verbose_name=_("Display order"), default=99, )

	def __str__(self):
		return self.internal_name

	@property
	def name(self, language_code=None):
		return self.lookup_translation(attr="name", language_code=language_code)

	@property
	def description(self, language_code=None):
		return self.lookup_translation(attr="description", language_code=language_code)

	class Meta:
		verbose_name = _("Product")
		verbose_name_plural = _("Product")


class ProductTranslation(MultilingualModel):
	product = models.ForeignKey(verbose_name=_("Product"), to=Product, related_name="translations", )
	name = models.CharField(verbose_name=_("Name"), max_length=256, )
	description = models.TextField(verbose_name=_("Description"), blank=True, null=True, )

	class Meta:
		verbose_name = _("Translation")
		verbose_name_plural = _("Translations")
		unique_together = ("product", "language_code")


class ProductMedia(Media):
	product = models.ForeignKey(verbose_name=_("Product"), to=Product, related_name="media", )

	class Meta:
		ordering = ["position", "-created", ]
		verbose_name = _('Product Media')
		verbose_name_plural = _('Product Media')
