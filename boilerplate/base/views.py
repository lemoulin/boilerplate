from django.views.generic import TemplateView
from lemoulin_extras.dynamic_settings import dynamic_settings

from . import models


class Home(TemplateView):
	template_name = "base/home.html"

	def get_context_data(self, **kwargs):
		context = super(Home, self).get_context_data(**kwargs)

		DYNAMIC_SETTINGS = dynamic_settings.dynamic_settings
		DYNAMIC_SETTINGS["page_title"] = "Star Wars"
		DYNAMIC_SETTINGS["GOOGLE_ANALYTICS_CODE"] = "ga_999"

		context["products"] = models.Product.objects.all()
		context["DYNAMIC_SETTINGS"] = DYNAMIC_SETTINGS

		return context
