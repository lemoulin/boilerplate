# Signals and receivers
from django.db.models.signals import pre_save
from django.dispatch import receiver

from . import models


@receiver(pre_save, sender=models.ProductMedia)
def set_Ppia_data(sender, instance, **kwargs):
	"""Sets Product Media Data"""

	instance._set_media_data()
