from django.contrib import admin
from django.conf import settings

from lemoulin_extras.gallery.admin import MediaAdminInline

from . import models


class ProductTranslation_Inline(admin.StackedInline):
	model = models.ProductTranslation
	extra = len(settings.LANGUAGES)
	max_num = len(settings.LANGUAGES)


class ProductMedia_Inline(MediaAdminInline):
	model = models.ProductMedia
	# extra = 0
	# fields = ['title', 'image', 'admim_thumbnail_preview', 'image_base64', 'image_filename', 'position', ]
	# fieldsets = None
	# readonly_fields = ('admim_thumbnail_preview',)


@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
	list_display = ("internal_name", "order", )
	ordering = ["order", ]
	search_fields = ["internal_name", ]
	prepopulated_fields = {"slug": ["internal_name"]}

	inlines = [
		ProductTranslation_Inline,
		ProductMedia_Inline,
	]

	class Media:
		js = (
			settings.GRAPPELLI_URL + 'tinymce/jscripts/tiny_mce/tiny_mce.js',
			settings.GRAPPELLI_URL + 'tinymce_setup/tinymce_setup.js'
		)
