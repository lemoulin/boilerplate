#!/bin/bash

VIRTUALENVDIR=/srv/.virtualenvs/boilerplate			# Virtualenv directory
DJANGODIR=/srv/www/boilerplate						# Django project directory
SUPERVISORPROCESS=boilerplate

echo "Activating source $VIRTUALENVDIR/bin/activate"
source $VIRTUALENVDIR/bin/activate
sleep 5 & wait		# Sleep here to make sure source is activated
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

echo "Syncing DB"
exec $DJANGODIR/manage.py syncdb & wait

echo "Migrating DB"
exec $DJANGODIR/manage.py migrate & wait

echo "Restarting process"
exec supervisorctl restart $SUPERVISORPROCESS & wait

echo "Clearing cache"
exec $DJANGODIR/manage.py clear_cache & wait

echo "Done"
