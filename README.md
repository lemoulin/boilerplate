# Boilerplate Django Template

## Requirements

- Python 3.4
- Django 1.9

## Versions

- 1.0


## Installation

See `docs/install.md`
